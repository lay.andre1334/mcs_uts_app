package com.example.uts_mcs_app;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Vector;

public class SpendingAdapter extends RecyclerView.Adapter<SpendingAdapter.ViewHolder> {

    Context ctx;
    Vector<Spending> vecSpending = new Vector<>();
    public static final String SEND_ID = "com.example.application.UTS_MCS_APP.SEND_ID";
    public SpendingAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void setVecSpending(Vector<Spending> vecSpending) {
        this.vecSpending = vecSpending;
    }

    @NonNull
    @Override
    public SpendingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.spending_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpendingAdapter.ViewHolder holder, int position) {
        holder.tvName.setText(vecSpending.get(position).getName());
        holder.tvNominal.setText("Rp. " + vecSpending.get(position).getNominal());
        holder.tvDate.setText(vecSpending.get(position).getDate());

        holder.cvSpending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, EditItemActivity.class);
                intent.putExtra(SEND_ID,vecSpending.get(position).getId());
                ctx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return vecSpending.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvNominal, tvDate;
        CardView cvSpending;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvNominal = itemView.findViewById(R.id.tvNominal);
            tvDate = itemView.findViewById(R.id.tvDate);
            cvSpending = itemView.findViewById(R.id.cvSpending);
        }
    }
}
