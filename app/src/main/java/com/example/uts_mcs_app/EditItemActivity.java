package com.example.uts_mcs_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EditItemActivity extends AppCompatActivity {

    EditText txtEditName, txtEditNominal;
    Button btnEdit;
    private String name, nominal, currDate;
    private int id;
    public static final String SEND_ID = "com.example.application.UTS_MCS_APP.SEND_ID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        txtEditName = findViewById(R.id.txtEditName);
        txtEditNominal = findViewById(R.id.txtEditNominal);
        btnEdit = findViewById(R.id.btnEdit);

        Intent intent = getIntent();
        id = intent.getIntExtra(SEND_ID,0);

        DBHelper db = new DBHelper(this);
        Spending spending = db.getSpendingDetail(id);

        txtEditName.setText(spending.getName());
        txtEditNominal.setText(spending.getNominal());


        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag = false;
                name = txtEditName.getText().toString();
                if(name.isEmpty()){
                    txtEditName.setError("Please fill the name");
                    flag = true;
                }

                nominal = txtEditNominal.getText().toString();
                if(nominal.isEmpty()){
                    txtEditNominal.setError("Please fill the nominal");
                    flag = true;
                }

                currDate = new SimpleDateFormat("dd MMMM yyyy").format(Calendar.getInstance().getTime());

                if(flag){
                    Toast.makeText(getBaseContext(),"Please fill out the field", Toast.LENGTH_LONG).show();
                }else{
                    //insert into DB
                    DBHelper db = new DBHelper(getBaseContext());
                    db.updateSpending(id, name, nominal);
                    Toast.makeText(getBaseContext(),"Data Updated",Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}