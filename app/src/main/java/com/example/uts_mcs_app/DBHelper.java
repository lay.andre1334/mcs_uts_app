package com.example.uts_mcs_app;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class DBHelper extends SQLiteOpenHelper {

        //db info
        public static final String DATABASE_NAME = "SpendingDB.db";
        public static final Integer DATABASE_VERSION = 1;

        //table column
        public static final String TABLE_NAME = "tblSpending";
        public static final String TABLE_COLUMN_ID = "id";
        public static final String TABLE_COLUMN_NAME = "spending_name";
        public static final String TABLE_COLUMN_NOMINAL = "spending_nominal";
        public static final String TABLE_COLUMN_DATE = "spending_date";

        public DBHelper(@Nullable Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String create_table_query = "CREATE TABLE " + TABLE_NAME + "( " +
                    TABLE_COLUMN_ID +  " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TABLE_COLUMN_NAME + " TEXT NOT NULL, " +
                    TABLE_COLUMN_NOMINAL  + " TEXT NOT NULL, " +
                    TABLE_COLUMN_DATE + " INTEGER NOT NULL)";
            db.execSQL(create_table_query);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }

        public void insertSpending(String name, String nominal, String date){
            SQLiteDatabase db = this.getWritableDatabase();
            String insert_query = "INSERT INTO " + TABLE_NAME +
                    "(" + TABLE_COLUMN_NAME + ", " + TABLE_COLUMN_NOMINAL + ", " +TABLE_COLUMN_DATE + ")" +
                    "VALUES('" + name + "', '" + nominal + "', '" + date + "')";

           db.execSQL(insert_query);
           db.close();
           this.close();
        }

        public void updateSpending(int id, String name, String nominal){
            SQLiteDatabase db = this.getWritableDatabase();
            String update_query = "UPDATE " + TABLE_NAME + " SET " +
                    TABLE_COLUMN_NAME + " = '" + name + "', " +
                    TABLE_COLUMN_NOMINAL + " = '" + nominal + "' " +
                    "WHERE " + TABLE_COLUMN_ID + " = " + id;
            db.execSQL(update_query);
            db.close();
            this.close();

        }

        public Vector<Spending> getSpending(){
            SQLiteDatabase db = this.getReadableDatabase();

            String get_spending_query = "SELECT * FROM " + TABLE_NAME;
            Cursor cursor = db.rawQuery(get_spending_query, null);

            Vector<Spending> vecSpending = new Vector<>();


                if(cursor.moveToFirst()){
                    do{
                        Integer id = cursor.getInt(0);
                        String name = cursor.getString(1);
                        String nominal = cursor.getString(2);
                        String date = cursor.getString(3);
                        Spending spend = new Spending(id,name,nominal,date);
                        vecSpending.add(spend);
                    }while(cursor.moveToNext());
                }

            cursor.close();
            db.close();
            this.close();
            return vecSpending;
        }

        public Spending getSpendingDetail(int id){
            SQLiteDatabase db = this.getReadableDatabase();

            String get_spending_detail_query = "SELECT * FROM " + TABLE_NAME + " WHERE " +
                    TABLE_COLUMN_ID + " = " + id;
            Cursor cursor = db.rawQuery(get_spending_detail_query, null);

            if(cursor.moveToFirst()){
                Integer spendId = cursor.getInt(0);
                String name = cursor.getString(1);
                String nominal = cursor.getString(2);
                String date = cursor.getString(3);
                Spending spending = new Spending(spendId,name,nominal,date);
                cursor.close();
                db.close();
                this.close();
                return spending;
            }

            cursor.close();
            db.close();
            this.close();
            return null;
        }


}
