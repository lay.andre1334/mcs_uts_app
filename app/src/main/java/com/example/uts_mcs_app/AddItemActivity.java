package com.example.uts_mcs_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddItemActivity extends AppCompatActivity {

    EditText txtName, txtNominal;
    Button btnAdd;
    private int date, month, year;
    private String name, nominal, currDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        txtName = findViewById(R.id.txtName);
        txtNominal = findViewById(R.id.txtNominal);
        btnAdd = findViewById(R.id.btnAdd);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag = false;
                name = txtName.getText().toString();
                if(name.isEmpty()){
                    txtName.setError("Please fill the name");
                    flag = true;
                }

                nominal = txtNominal.getText().toString();
                if(nominal.isEmpty()){
                    txtNominal.setError("Please fill the nominal");
                    flag = true;
                }

                currDate = new SimpleDateFormat("dd MMMM yyyy").format(Calendar.getInstance().getTime());

                if(flag){
                    Toast.makeText(getBaseContext(),"Please fill out the field", Toast.LENGTH_LONG).show();
                }else{
                    //insert into DB
                    DBHelper db = new DBHelper(getBaseContext());
                    db.insertSpending(name, nominal, currDate);
                    finish();
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }
}