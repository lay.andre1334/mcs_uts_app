package com.example.uts_mcs_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    Vector<Spending> vecSpending = new Vector<>();
    RecyclerView rvSpending;
    SpendingAdapter spendingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getRvData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getRvData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent(this, AddItemActivity.class);
        startActivity(intent);
        return true;
    }

    public void getRvData(){
        rvSpending = findViewById(R.id.rvSpending);
        DBHelper db = new DBHelper(this);
        vecSpending = db.getSpending();

        SpendingAdapter spendingAdapter = new SpendingAdapter(this);
        spendingAdapter.setVecSpending(vecSpending);

        rvSpending.setAdapter(spendingAdapter);
        rvSpending.setLayoutManager(new GridLayoutManager(this, 1));
    }
}